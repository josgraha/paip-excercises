# PAIP Exercises

## Description
Collection of exercises from the seminal work "Paradigms of Artificial Intelligence" by Dr. Peter Norvig.  This work is grouped into folders which represent the sections of the book.

## Running
The instructions for building and running the exercises are in the README of the respective folder.